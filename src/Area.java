public class Area {
    public double calculate(Integer n) {
        if (n % 2 == 0) {
            return Math.pow(n, 2);
        } else if (n % 3 == 0) {
            return Math.PI * Math.pow(n, 2);
        } else {
            return Math.sqrt(3) * Math.pow(n, 2) / 4;
        }
    }
}
